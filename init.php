<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
function initSamsonSync(): string
{
    include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/samson/class.samsonsync.php");

    $samsonSync = new SamsonSync();
    $samsonSync->init();

    return "initSamsonSync();";
}

function processSamsonSync(): string
{
    include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/samson/class.samsonsync.php");

    $pauseStart = DateTime::createFromFormat('H:i', "09:00");
    $pauseEnd = DateTime::createFromFormat('H:i', "21:00");
    $now = new DateTime('NOW');

    if ($now < $pauseStart || $now > $pauseEnd) {
        $samsonSync = new SamsonSync();
        $samsonSync->process();
    }

    return "processSamsonSync();";
}

?>