<?

/**
 * Класс для загрузки товарной номенклатуры по API оптового поставщика канцтоваров «Самсон»
 * в каталог сайта на Битриксе.
 *
 * https://api.samsonopt.ru/v1/doc/index.html
 *
 */
class SamsonSync
{
    // Samson options
    const API_KEY = "";
    static $arHeaderList = ['Accept: application/json', 'User-Agent: string'];
    // Iblock options
    const CATALOG_IBLOCK_ID = 1;
    const BASE_STORE_ID = 1;
    const MODIFY_USER_ID = 1;
    const MORE_PHOTO_PROPERTY_CODE = 'MORE_PHOTO';
    const VAT_ID = 3;
    const STORE_IDP = 4;
    const STORE_TRANSIT = 5;
    const STORE_DISTRIBUTION_WAREHOUSE = 6;
    // settings
    const EXECUTE_TIME_LIMIT = 30;
    static $steps = [
        'init',
        'loadCategories',
        'loadAssortment',
        'update',
        'disable',
        'add',
        'double',
        'sitemap',
        'reindex',
        'complete'
    ];
    // price settings
    static $skuPriceCoef = ['110086' => 2.5, '110071' => 2.5, '111788' => 55];
    static $sectionPriceCoef = [
        '599' => 107, // Ежедневники, календари, планинги 2020
        '773' => 107, // Офисные доски и демонстрационное оборудование
        '1683' => 61, // Картриджи
        '625' => 107, // Бумага для офисной техники
        '626' => 32, //  → Бумага для принтера А3, А4, А5
        '1353' => 107, // Этикетки, наклейки самоклеящиеся
        '1120' => 107, // Товары для художников
        '1032' => 107, // Товары для школы
        '908' => 107, // Папки и архивные системы
        '848' => 107, // Офисные принадлежности
        '927' => 107, // Письменные принадлежности
        '1' => 107, // Бумажная продукция
        '761' => 107, // Деловые и подарочные аксессуары
        '765' => 84, //  → Настольные наборы и аксессуары
        '1232' => 73, // Техника для офиса
        '1413' => 73, // Техника бытовая
        '1438' => 55, //  → Сушилки для рук
        '818' => 73, // Мебель для офиса
        '824' => 73, //  → Офисные кресла
        '842' => 61, //  → Шкафчики для ключей, аптечки
        '1043' => 73, // Хозяйственные, гигиенические товары
        '1240' => 73, // Продукты питания и посуда
        '1609' => 73, // Инструменты и ремонт
        '1819' => 73, // Инвентарь и хозяйственные принадлежности
        '1820' => 107, // Новый год
    ];

    private $step;
    private $timestamp;
    private $currentTimestamp;
    private $nextParam;
    private $samsonTree;
    private $connectedCategory;
    private $sectionChain;
    private $catalogProperties;
    private $catalogEnumValues;
    private $arTranslit;
    private $arSectTranslit;
    private $ignore;
    private $ignoreBrand;
    private $sitemap;
    private $reindex;
    private $tmp_dir;
    private $stateFile;
    private $reportDir;
    private $logDir;
    private $reportTemplate;
    private $sectLinkDatafile;
    private $rootSectLinkDatafile;
    private $ignoreFile;
    private $ignoreBrandFile;
    private $workFile = false;
    private $basePriceId;

    public function __construct(): void
    {
        $this->tmp_dir = $_SERVER["DOCUMENT_ROOT"] . "/upload/samson/tmp";
        $this->stateFile = $_SERVER["DOCUMENT_ROOT"] . "/upload/samson/tmp/state.dat";
        $this->reportDir = $_SERVER["DOCUMENT_ROOT"] . "/upload/samson/reports";
        $this->logDir = $_SERVER["DOCUMENT_ROOT"] . "/upload/samson/log";
        $this->reportTemplate = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/samson/report.html";
        $this->sectLinkDatafile = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/samson/sections-link.csv";
        $this->rootSectLinkDatafile = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/samson/sections-root-link.csv";
        $this->ignoreFile = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/samson/ignore.csv";
        $this->ignoreBrandFile = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/samson/ignore-brand.csv";
        $this->currentTimestamp = time();
        $this->workFile = false;
    }

    public function init(): void
    {
        if (!file_exists($this->stateFile)) {
            $params = $this->getInitState();
            $this->setStateFromParams($params);
            $this->saveState();
        }
    }

    public function process(): bool
    {
        $restore = $this->restoreState();
        if (!$restore) {
            // Процесс не инициализирован, выход
            return false;
        }

        $step = self::$steps[$this->step];

        switch ($step) {
            case 'init':
            {
                $this->loadIgnoreData();
                $this->setNextStep();
                break;
            }
            case 'loadCategories':
            {
                $res = $this->getSamsonCategory();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'loadAssortment':
            {
                $res = $this->getSamsonAssortment();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'update':
            {
                $res = $this->updateCatalogItems();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'disable':
            {
                $res = $this->disableCatalogItems();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'add':
            {
                $res = $this->addCatalogItems();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'double':
            {
                $res = $this->checkDeleteDouble();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'sitemap':
            {
                $res = $this->createSiteMap();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'reindex':
            {
                $res = $this->reindexSearch();
                if ($res) {
                    $this->setNextStep();
                }
                break;
            }
            case 'complete':
            {
                $this->saveReport();
                $this->saveUnplacedSections();
                $this->clear();
                break;
            }
        }

        if ($step != 'complete') {
            $this->saveState();
        }

        print_r('step: ' . ($step) . '<br />');
        print_r('exec time: ' . (time() - $this->currentTimestamp) . '<br />');

        return true;
    }

    private function abort(): void
    {
        $this->saveReportAbort();
        $this->clear();
        die;
    }

    private function getInitState(): array
    {
        return [
            "step" => 0,
            "timestamp" => time(),
            "nextParam" => '',
            "samsonTree" => [],
            "connectedCategory" => [],
            "sectionChain" => [],
            "catalogProperties" => [],
            "catalogEnumValues" => [],
            "arTranslit" => [],
            "arSectTranslit" => [],
            "basePriceId" => 0,
            "ignore" => [],
            "ignoreBrand" => [],
            "sitemap" => [],
            "reindex" => false,
        ];
    }

    private function setStateFromParams(array $params): void
    {
        $this->step = $params['step'];
        $this->timestamp = $params['timestamp'];
        $this->nextParam = $params['nextParam'];
        $this->samsonTree = $params['samsonTree'];
        $this->connectedCategory = $params['connectedCategory'];
        $this->sectionChain = $params['sectionChain'];
        $this->catalogProperties = $params['catalogProperties'];
        $this->catalogEnumValues = $params['catalogEnumValues'];
        $this->arTranslit = $params['arTranslit'];
        $this->arSectTranslit = $params['arSectTranslit'];
        $this->basePriceId = $params['basePriceId'];
        $this->ignore = $params['ignore'];
        $this->ignoreBrand = $params['ignoreBrand'];
        $this->sitemap = $params['sitemap'];
        $this->reindex = $params['reindex'];
    }

    private function restoreState(): bool
    {
        if (file_exists($this->stateFile)) {
            $content = file_get_contents($this->stateFile);
            $params = json_decode($content, true);

            $this->setStateFromParams($params);

            return true;
        }

        return false;
    }

    private function saveState(): void
    {
        $params = [
            "step" => $this->step,
            "timestamp" => $this->timestamp,
            "nextParam" => $this->nextParam,
            "samsonTree" => $this->samsonTree,
            "connectedCategory" => $this->connectedCategory,
            "sectionChain" => $this->sectionChain,
            "catalogProperties" => $this->catalogProperties,
            "catalogEnumValues" => $this->catalogEnumValues,
            "arTranslit" => $this->arTranslit,
            "arSectTranslit" => $this->arSectTranslit,
            "basePriceId" => $this->basePriceId,
            "ignore" => $this->ignore,
            "ignoreBrand" => $this->ignoreBrand,
            "sitemap" => $this->sitemap,
            "reindex" => $this->reindex,
        ];

        $content = json_encode($params, JSON_UNESCAPED_UNICODE);
        file_put_contents($this->stateFile, $content);
    }

    private function clear(): void
    {
        $this->clearState();
        $this->clearReports();
        $this->clearLogs();
    }

    private function clearState(): void
    {
        if (file_exists($this->stateFile)) {
            unlink($this->stateFile);
        }
    }

    private function clearReports(): void
    {
        $files = [];
        if ($handle = opendir($this->reportDir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file == "." || $file == ".." || strpos($file, 'report') === false) {
                    continue;
                }

                $files[filemtime($this->reportDir . '/' . $file)] = $file;
            }
            closedir($handle);
            // sort
            krsort($files);
            // delete
            $i = 0;
            foreach ($files as $time => $file) {
                if ($i < 5) {
                    $i++;
                    continue;
                }
                $fn = $this->reportDir . '/' . $file;
                unlink($fn);
            }
        }
    }

    private function clearLogs(): void
    {
        if ($handle = opendir($this->logDir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $fn = $this->logDir . '/' . $file;
                    unlink($fn);
                }
            }
            closedir($handle);
        }
    }

    private function setNextStep(): void
    {
        $this->step += 1;
    }

    private function saveLog(array $items, string $type): void
    {
        $fn = $this->getNextFileNumFileName($this->logDir, $type);

        $content = json_encode($items, JSON_UNESCAPED_UNICODE);
        file_put_contents($fn, $content, FILE_APPEND);
    }

    private function isLimitTime(): bool
    {
        return time() - $this->currentTimestamp >= self::EXECUTE_TIME_LIMIT;
    }

    private function getSamsonCategory(): bool
    {
        $url = $this->nextParam ?? 'https://api.samsonopt.ru/v1/category/?api_key=' . self::API_KEY;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, self::$arHeaderList);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5); //timeout in seconds
        $result = curl_exec($curl);
        curl_close($curl);

        $obj = json_decode($result, true);

        if (!$obj || !$obj['data']) {
            print_r('abort');
            $this->abort();
            return true;
        }

        $this->samsonTree = array_merge($this->samsonTree, $obj['data']);

        if (isset($obj['meta']) && isset($obj['meta']['pagination']) && isset($obj['meta']['pagination']['next'])) {
            $this->nextParam = $obj['meta']['pagination']['next'];
            return false;
        } else {
            $this->nextParam = '';
        }

        return true;
    }

    private function getSamsonAssortment(): bool
    {
        $allLoaded = false;
        while (!$this->isLimitTime() && !$allLoaded) {
            $url = $this->nextParam ?? 'https://api.samsonopt.ru/v1/assortment/?api_key=' . self::API_KEY
                . '&pagination_count=1000';

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, self::$arHeaderList);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10); //timeout in seconds
            $result = curl_exec($curl);
            curl_close($curl);

            $obj = json_decode($result, true);

            if (!$obj || !$obj['data']) {
                $this->abort();
                return true;
            }

            $parsed = parse_url($url);
            parse_str($parsed['query'], $query);
            $page = isset($query['pagination_page']) ? $query['pagination_page'] : 1;
            $pageStr = $this->pageNumToStr($page);

            $file = $this->tmp_dir . '/items' . $pageStr . '.dat';
            file_put_contents($file, json_encode($obj['data'], JSON_UNESCAPED_UNICODE), FILE_APPEND);

            if (isset($obj['meta']) && isset($obj['meta']['pagination']) && isset($obj['meta']['pagination']['next'])) {
                $this->nextParam = $obj['meta']['pagination']['next'];
            } else {
                $this->nextParam = '';
                $allLoaded = true;
            }
        }

        return $allLoaded;
    }

    private function pageNumToStr($page): string
    {
        return $page < 10 ? '0' . strval($page) : strval($page);
    }

    private function isItemIgnore($item): bool
    {
        if (in_array($item['sku'], $this->ignore)) {
            return true;
        }

        if (in_array(mb_strtolower($item['brand']), $this->ignoreBrand)) {
            return true;
        }

        return false;
    }

    private function updateCatalogItems(): bool
    {
        $items = $this->loadNextFile('items');
        $notFound = [];
        $disabled = [];
        $updated = [];
        $fullProcesed = true;
        if ($items !== false) {
            foreach ($items as $index => $item) {
                if ($this->isItemIgnore($item)) {
                    continue;
                }

                $updateRes = $this->updateItem($item);

                if ($updateRes['not_found']) {
                    $notFound[] = $item;
                } else {
                    if ($updateRes['disable']) {
                        $disabled[] = $this->formatResult($item, $updateRes);;
                    } else {
                        $updated[] = $this->formatResult($item, $updateRes);
                    }
                }

                // Проверка на время исполнения
                if ($this->isLimitTime()) {
                    $rest = array_slice($items, $index);
                    $this->updateWorkFile($rest);
                    $fullProcesed = false;
                    break;
                }
            }
            if ($fullProcesed) {
                $this->removeWorkFile();
            }
            print_r('update count: ' . count($updated) . '<br />');
            $this->saveLog($updated, 'update');
            if (count($notFound) > 0) {
                $this->saveNewItemToFile($notFound);
            }
            return false;
        }
        return true;
    }

    private function addCatalogItems(): bool
    {
        $items = $this->loadNextFile('new');
        $added = [];
        $fullProcesed = true;
        if ($items !== false) {
            foreach ($items as $index => $item) {
                $addRes = $this->addItem($item);
                if ($addRes) {
                    if ($addRes['status'] == 'success') {
                        $added[] = $this->formatResult($item, $addRes);
                    }
                }

                // Проверка на время исполнения
                if ($this->isLimitTime()) {
                    $rest = array_slice($items, $index);
                    $this->updateWorkFile($rest);
                    $fullProcesed = false;
                    break;
                }
            }
            if ($fullProcesed) {
                $this->removeWorkFile();
            }

            $this->saveLog($added, 'add');
            return false;
        }
        return true;
    }

    private function loadNextFile($prefix): ?object
    {
        $list = scandir($this->tmp_dir);
        $itemsFile = '';
        foreach ($list as $key => $fn) {
            if (strpos($fn, $prefix) !== false) {
                $itemsFile = $fn;
                break;
            }
        }
        if ($itemsFile != '') {
            $fileName = $this->tmp_dir . '/' . $itemsFile;
            $this->workFile = $fileName;

            $content = file_get_contents($fileName);
            $obj = json_decode($content, true);

            return $obj;
        }
        return null;
    }

    private function updateWorkFile($data): void
    {
        if ($this->workFile) {
            $content = json_encode($data, JSON_UNESCAPED_UNICODE);
            file_put_contents($this->workFile, $content);
            $this->workFile = false;
        }
    }

    private function removeWorkFile(): void
    {
        if ($this->workFile && file_exists($this->workFile)) {
            unlink($this->workFile);
        }
    }

    private function getNextFileNumFileName(string $dir, string $prefix): string
    {
        $i = 0;
        do {
            $i++;
            $num = $this->pageNumToStr($i);
            $fileName = $dir . '/' . $prefix . $num . '.dat';
        } while (file_exists($fileName));
        return $fileName;
    }

    private function saveNewItemToFile($arItems): void
    {
        $fileName = $this->getNextFileNumFileName($this->tmp_dir, "new");

        $content = json_encode($arItems, JSON_UNESCAPED_UNICODE);
        file_put_contents($fileName, $content, FILE_APPEND);
    }

    private function updateItem(array $item): ?array
    {
        if (!CModule::IncludeModule('iblock') || !CModule::IncludeModule('catalog')) {
            return null;
        }

        $article = [$item['sku'] . 'C', $item['sku'] . 'С'];
        $arFilter = ["IBLOCK_ID" => self::CATALOG_IBLOCK_ID, "PROPERTY_CML2_ARTICLE" => $article];
        $arSelect = ["ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PROPERTY_CML2_ARTICLE"];
        $rsElement = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        if ($arElement = $rsElement->GetNext()) {
            $ELEMENT_ID = $arElement['ID'];

            $storeUpdateRes = false;
            $priceUpdateRes = false;

            // Остатки
            $arProductFields = $this->createCatalogProductArray($ELEMENT_ID, $item);
            $isAvailable = $arProductFields['QUANTITY'] > 0;
            if ($isAvailable) {
                $obProduct = new CCatalogProduct();
                $storeUpdateRes = $obProduct->Update($ELEMENT_ID, $arProductFields);
            }

            $stores = $this->mapTypeValue($item['stock_list']);
            $arFields = ["PRODUCT_ID" => $ELEMENT_ID, "STORE_ID" => self::STORE_IDP, "AMOUNT" => $stores['idp']];
            CCatalogStoreProduct::UpdateFromForm($arFields);
            $arFields = [
                "PRODUCT_ID" => $ELEMENT_ID,
                "STORE_ID" => self::STORE_TRANSIT,
                "AMOUNT" => $stores['transit']
            ];
            CCatalogStoreProduct::UpdateFromForm($arFields);
            $arFields = [
                "PRODUCT_ID" => $ELEMENT_ID,
                "STORE_ID" => self::STORE_DISTRIBUTION_WAREHOUSE,
                "AMOUNT" => $stores['distribution_warehouse']
            ];
            CCatalogStoreProduct::UpdateFromForm($arFields);

            // Цены
            if ($isAvailable) {
                $arPriceFields = $this->createCatalogPriceArray($ELEMENT_ID, $arElement['IBLOCK_SECTION_ID'], $item);

                $priceID = $this->getBasePriceId();
                $res = CPrice::GetList([], ["PRODUCT_ID" => $ELEMENT_ID, "CATALOG_GROUP_ID" => $priceID]);
                if ($arr = $res->Fetch()) {
                    $priceUpdateRes = CPrice::Update($arr["ID"], $arPriceFields);
                } else {
                    $priceUpdateRes = CPrice::Add($arPriceFields);
                }
            }

            // Обновление
            $isActive = $isAvailable && $arElement["IBLOCK_SECTION_ID"];
            $el = new CIBlockElement;

            $arLoadProductArray = [];
            if ($item['name'] != $arElement["~NAME"]) {
                $arLoadProductArray = $this->createCatalogItemArray($item);
                unset($arLoadProductArray["CODE"]);
                unset($arLoadProductArray["IBLOCK_SECTION_ID"]);
                if (!$arLoadProductArray['DETAIL_PICTURE']) {
                    unset($arLoadProductArray["DETAIL_PICTURE"]);
                }
            }

            $arLoadProductArray = array_merge($arLoadProductArray, [
                "MODIFIED_BY" => self::MODIFY_USER_ID,
                "ACTIVE" => $isActive ? "Y" : "N"
            ]);
            // Обновление раздела
            if (!$arElement['IBLOCK_SECTION_ID']) {
                $arLoadProductArray['IBLOCK_SECTION_ID'] = $this->resolveSamsonSection($item['category_list']);
                if ($arLoadProductArray['IBLOCK_SECTION_ID']) {
                    $this->connectCategory($item['category_list'], $arLoadProductArray['IBLOCK_SECTION_ID']);
                } else {
                    // Привязка к корневому раздел
                    foreach ($item['category_list'] as $catID) {
                        $rootSectID = $this->resolveRootSectByData;
                        if ($rootSectID) {
                            $arLoadProductArray['IBLOCK_SECTION_ID'] = $rootSectID;
                            break;
                        }
                    }
                }
            } else {
                $this->connectCategory($item['category_list'], $arElement['IBLOCK_SECTION_ID']);
            }

            $res = $el->Update($ELEMENT_ID, $arLoadProductArray, false, false);
            $err = $el->LAST_ERROR;
            unset($el);

            $status = !$res ? 'error' : (!$isActive
                ? 'disable'
                : (($storeUpdateRes !== false && $priceUpdateRes !== false) ? 'success' : 'error')
            );

            $arRes = array(
                "ID" => $ELEMENT_ID,
                "IBLOCK_SECTION_ID" => $arElement["IBLOCK_SECTION_ID"],
                "DETAIL_PAGE_URL" => $arElement['DETAIL_PAGE_URL'],
                "PRICE" => $arPriceFields['PRICE'],
                "QUANTITY" => $arProductFields['QUANTITY'],
                "ARTICLE" => $arElement["PROPERTY_CML2_ARTICLE_VALUE"],
                "status" => $status,
                "msg" => $err . (!$isActive ? (!$isAvailable ? 'Нет в наличии' : 'Не найден раздел') : ''),
                "store_msg" => $storeUpdateRes !== false ? '' : 'Ошибка обновления остатков',
                "price_msg" => $priceUpdateRes !== false ? '' : 'Ошибка обновления цены',
            );
            return $arRes;
        }
        return [
            "status" => "error",
            "not_found" => true,
            "msg" => "Елемент не найден"
        ];
    }

    private function addItem(array $item): ?array
    {
        if (!CModule::IncludeModule('iblock') || !CModule::IncludeModule('catalog')) {
            return null;
        }

        $el = new CIBlockElement;
        $arLoadProductArray = $this->createCatalogItemArray($item);
        if (!$arLoadProductArray['DETAIL_PICTURE']) {
            return null;
        }

        $tmp = $this->createCatalogProductArray(0, $item);
        if ($tmp['QUANTITY'] <= 0) {
            return null;
        }

        // Привязка к разделу
        if ($arLoadProductArray['IBLOCK_SECTION_ID']) {
            $this->connectCategory($item['category_list'], $arLoadProductArray['IBLOCK_SECTION_ID']);
        } else {
            // Привязка к корневому раздел
            foreach ($item['category_list'] as $catID) {
                $rootSectID = $this->resolveRootSectByData;
                if ($rootSectID) {
                    $arLoadProductArray['IBLOCK_SECTION_ID'] = $rootSectID;
                    break;
                }
            }
        }

        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arProductFields = $this->createCatalogProductArray($PRODUCT_ID, $item);

            $prodCreateRes = CCatalogProduct::Add($arProductFields);
            if (!$prodCreateRes) {
                $error = [
                    "status" => "error",
                    "msg" => "Ошибка добавления параметров товара"
                ];
                return $error;
            }

            $stores = $this->mapTypeValue($item['stock_list']);
            $arFields = ["PRODUCT_ID" => $PRODUCT_ID, "STORE_ID" => self::STORE_IDP, "AMOUNT" => $stores['idp']];
            CCatalogStoreProduct::UpdateFromForm($arFields);
            $arFields = [
                "PRODUCT_ID" => $PRODUCT_ID,
                "STORE_ID" => self::STORE_TRANSIT,
                "AMOUNT" => $stores['transit']
            ];
            CCatalogStoreProduct::UpdateFromForm($arFields);
            $arFields = [
                "PRODUCT_ID" => $PRODUCT_ID,
                "STORE_ID" => self::STORE_DISTRIBUTION_WAREHOUSE,
                "AMOUNT" => $stores['distribution_warehouse']
            ];
            CCatalogStoreProduct::UpdateFromForm($arFields);

            $arPriceFields = $this->createCatalogPriceArray(
                $PRODUCT_ID,
                $arLoadProductArray['IBLOCK_SECTION_ID'],
                $item
            );
            $priceCreateRes = CPrice::Add($arPriceFields);
            if (!$priceCreateRes) {
                $error = [
                    "status" => "error",
                    "msg" => "Ошибка создания цены товара"
                ];
                return $error;
            }

            // get page url
            $arFilter = [
                "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                "ID" => $PRODUCT_ID
            ];
            $arSelect = ["ID", "DETAIL_PAGE_URL"];
            $rsElement = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            $arNewItemFields = $rsElement->GetNext();

            return [
                "status" => "success",
                "ELEMENT_ID" => $PRODUCT_ID,
                "IBLOCK_SECTION_ID" => $arLoadProductArray["IBLOCK_SECTION_ID"],
                "DETAIL_PAGE_URL" => $arNewItemFields["DETAIL_PAGE_URL"],
                "PRICE" => $arPriceFields['PRICE'],
                "QUANTITY" => $arProductFields['QUANTITY'],
            ];
        } else {
            $error = [
                "status" => "error",
                "msg" => $el->LAST_ERROR
            ];
            return $error;
        }
    }

    private function disableCatalogItems(): bool
    {
        if (!CModule::IncludeModule('iblock')) {
            return false;
        }

        $pattern = '/^\d{6}(C|С)$/';
        $disabled = [];

        $article = ['%C', '%С'];
        $arFilter = [
            "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
            "PROPERTY_CML2_ARTICLE" => $article,
            "DATE_MODIFY_TO" => ConvertTimeStamp($this->timestamp, "FULL"),
            "ACTIVE" => "Y",
        ];
        $arSelect = ["ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_CML2_ARTICLE"];
        $fullProcesed = true;
        $rsElement = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($arElement = $rsElement->GetNext()) {
            $tmpArt = str_replace('С', '', $arElement['PROPERTY_CML2_ARTICLE_VALUE']);
            if (in_array($tmpArt, $this->ignore)) {
                continue;
            }

            $test = preg_match($pattern, $arElement['PROPERTY_CML2_ARTICLE_VALUE']);
            if ($test !== 0) {
                $arLoadProductArray = [
                    "MODIFIED_BY" => self::MODIFY_USER_ID,
                    "ACTIVE" => "N",
                ];
                $el = new CIBlockElement;
                $res = $el->Update($arElement["ID"], $arLoadProductArray);

                $item = [
                    "name" => $arElement["NAME"],
                    "ARTICLE" => $arElement['PROPERTY_CML2_ARTICLE_VALUE'],
                ];

                if ($res) {
                    $disableRes = [
                        'status' => 'success',
                        "ID" => $arElement["ID"],
                        "IBLOCK_SECTION_ID" => $arElement["IBLOCK_SECTION_ID"],
                        "msg" => "Не найден в выгрузке"
                    ];
                } else {
                    $disableRes = ['status' => 'error', 'msg' => $el->LAST_ERROR];
                }

                $disabled[] = $this->formatResult($item, $disableRes);
            }

            if ($this->isLimitTime()) {
                $fullProcesed = false;
                break;
            }
        }

        $this->saveLog($disabled, 'disable');
        return $fullProcesed;
    }

    private function checkDeleteDouble(): bool
    {
        if (!CModule::IncludeModule('iblock')) {
            return false;
        }

        $arSelect = ["ID", "IBLOCK_SECTION_ID", "NAME", "PROPERTY_CML2_ARTICLE"];
        $arFilter = [
            "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
            "PROPERTY_CML2_ARTICLE" => ['%C', '%С'],
            "CREATED_BY" => self::MODIFY_USER_ID
        ];
        $arGroup = ["PROPERTY_CML2_ARTICLE"];
        $res = CIBlockElement::GetList(["cnt" => "DESC"], $arFilter, $arGroup, false, $arSelect);

        $fullProcesed = true;
        while ($arFields = $res->GetNext()) {
            if ($arFields['CNT'] < 2) {
                break;
            }

            $article = $arFields['PROPERTY_CML2_ARTICLE_VALUE'];
            $count = $arFields['CNT'];

            $dFilter = [
                "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                "PROPERTY_CML2_ARTICLE" => $article,
                "CREATED_BY" => self::MODIFY_USER_ID
            ];
            $dSelect = ["ID", "TIMESTAMP_X"];
            $rs = CIBlockElement::GetList(["TIMESTAMP_X" => "DESC"], $dFilter, false, false, $dSelect);
            for ($i = 1; $i < $count; $i++) {
                $double = $rs->GetNext();
                CIBlockElement::Delete($double['ID']);
            }

            if ($this->isLimitTime()) {
                $fullProcesed = false;
                break;
            }
        }
        return $fullProcesed;
    }

    private function createSiteMap(): bool
    {
        include_once(dirname(__FILE__) . "/sitemap.php");

        $complete = false;
        while (!$complete) {
            $state = $this->sitemap;
            $sitemap = new Sitemap($state);
            $complete = $sitemap->process();
            $this->sitemap = $sitemap->getState();

            if ($this->isLimitTime()) {
                break;
            }
        }

        return $complete;
    }

    private function reindexSearch(): bool
    {
        CModule::IncludeModule('search');
        $NS = $this->reindex;
        if (!$NS) {
            $NS = CSearch::ReindexModule("iblock", false);
        }
        if (intVal($NS) > 1000) {
            return true;
        }

        $complete = false;
        while (!$complete && is_array($NS) && count($NS) > 0) {
            $NS = CSearch::ReIndexAll(false, self::EXECUTE_TIME_LIMIT, $NS);
            $this->reindex = $NS;
            if (!is_array($NS)) {
                $complete = true;
            }
            if ($this->isLimitTime()) {
                break;
            }
        }
        return $complete;
    }

    private function connectCategory(array $samsonCatList, int $sectionID): void
    {
        foreach ($samsonCatList as $catID) {
            foreach ($this->samsonTree as $key => $category) {
                if ($category['id'] == $catID) {
                    $this->samsonTree[$key]['IBLOCK_SECTION_ID'] = $sectionID;
                    if (isset($this->connectedCategory[$catID])) {
                        if (!in_array($sectionID, $this->connectedCategory[$catID])) {
                            $this->connectedCategory[$catID][] = $sectionID;
                        }
                    } else {
                        $this->connectedCategory[$catID] = [$sectionID];
                    }
                    break;
                }
            }
        }
    }

    private function createCatalogItemArray(array $item): array
    {
        $sectionID = $this->resolveSamsonSection($item['category_list']);
        $properties = $this->convertSamsonProperties($item);

        $arTranslit = $this->getTranslitOptions();
        $translitOpt = [
            "max_len" => $arTranslit["TRANS_LEN"],
            "change_case" => $arTranslit["TRANS_CASE"],
            "replace_space" => $arTranslit["TRANS_SPACE"],
            "replace_other" => $arTranslit["TRANS_OTHER"],
        ];
        $code = Cutil::translit($item['name'], "ru", $translitOpt);

        $product = [
            'MODIFIED_BY' => self::MODIFY_USER_ID,
            'CREATED_BY' => self::MODIFY_USER_ID,
            'IBLOCK_ID' => self::CATALOG_IBLOCK_ID,
            'NAME' => $item['name'],
            'CODE' => $code,
            'ACTIVE' => 'Y',
            'PREVIEW_TEXT' => $item['name'],
            'DETAIL_TEXT' => $item['description'] . "\n\n" . implode("\n", $item['characteristic_list']),
            'IBLOCK_SECTION_ID' => $sectionID,
            'PROPERTY_VALUES' => $properties,
            'DETAIL_PICTURE' => isset($item['photo_list']) && count($item['photo_list']) ? CFile::MakeFileArray(
                $item['photo_list'][0]
            ) : false,
        ];
        return $product;
    }

    private function mapTypeValue(array $arr): array
    {
        $res = [];
        foreach ($arr as $key => $value) {
            $res[$value['type']] = $value['value'];
        }
        return $res;
    }

    private function findPropByName(string $name, array $props)
    {
        foreach ($props as $prop) {
            if ($name == $prop['name']) {
                return $prop['value'];
            }
        }
        return null;
    }

    private function createCatalogProductArray(int $PRODUCT_ID, array $item): array
    {
        $stores = $this->mapTypeValue($item['stock_list']);
        $length = $this->findPropByName('Длина', $item['facet_list']);
        $width = $this->findPropByName('Ширина', $item['facet_list']);
        $height = $this->findPropByName('Высота', $item['facet_list']);

        $arProduct = [
            'ID' => $PRODUCT_ID,
            'QUANTITY' => $stores['idp'] + $stores['distribution_warehouse'] + $stores['transit'],
            'WEIGHT' => $item['weight'] * 1000,
            'LENGTH' => $length,
            'WIDTH' => $width,
            'HEIGHT' => $height,
            'PURCHASING_CURRENCY' => 'RUB',
            'VAT_ID' => self::VAT_ID,
            'VAT_INCLUDED' => 'Y'
        ];

        return $arProduct;
    }

    private function createCatalogPriceArray(int $PRODUCT_ID, int $SECTION_ID, array $item): array
    {
        $priceID = $this->getBasePriceId();
        $arPriceFields = [
            "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $priceID,
            "PRICE" => $this->calcPrice($item, $SECTION_ID),
            "CURRENCY" => 'RUB',
        ];

        return $arPriceFields;
    }

    private function calcPrice(array $item, ?int $SECTION_ID = null): float
    {
        $prices = $this->mapTypeValue($item['price_list']);

        $coef = $this->getPriceCoef($SECTION_ID, $item['sku']);
        if ($coef === false) {
            return floatval($prices['infiltration']);
        }

        return floatval($prices['contract'] + $prices['contract'] * $coef / 100);
    }

    private function getPriceCoef(int $sectID, string $sku): ?float
    {
        if (isset(self::$skuPriceCoef[$sku])) {
            return floatval(self::$skuPriceCoef[$sku]);
        }

        $path = $this->getSectionsIdCnain($sectID);
        if (count($path)) {
            $path = array_reverse($path);
            foreach ($path as $section) {
                if (isset(self::$sectionPriceCoef[$section])) {
                    return floatval(self::$sectionPriceCoef[$section]);
                }
            }
        }

        return null;
    }

    private function resolveSamsonSection(array $arCategory): ?int
    {
        foreach ($arCategory as $categoryID) {
            if (isset($this->connectedCategory[$categoryID])) {
                return $this->connectedCategory[$categoryID][0];
            }
        }
        $i = 0;
        $maxCount = count($arCategory) * count($this->samsonTree);
        foreach ($arCategory as $categoryID) {
            $id = $categoryID;
            do {
                foreach ($this->samsonTree as $key => $cat) {
                    if ($cat["id"] == $id) {
                        if (isset($cat["IBLOCK_SECTION_ID"])) {
                            return $cat["IBLOCK_SECTION_ID"];
                        }
                        $id = $cat["parent_id"];
                        break;
                    }
                    $i++;
                }
            } while ($id != 0 && $i <= $maxCount);
        }

        foreach ($arCategory as $categoryID) {
            $resolveID = $this->resolveSectByData($categoryID);
            if ($resolveID) {
                return $resolveID;
            }
        }

        return null;
    }

    public function tmpResolvePublic(int $sectID): void
    {
        $this->resolveSectByData($sectID);
    }

    private function resolveSectByData(int $sectID): ?int
    {
        if (!isset($this->arSectLink)) {
            $this->arSectLink = $this->loadSectLinkData();
        }

        $log = '';
        print_r('--------------------' . "<br />\n");
        $log .= '--------------------' . "<br />\n";
        print_r('try resolve: ' . $sectID . "<br />\n");
        $log .= 'try resolve: ' . $sectID . "<br />\n";

        $arSectLink = $this->arSectLink;
        if (!isset($arSectLink[$sectID])) {
            print_r('category not found: ' . $sectID . "<br />\n");
            $log .= 'category not found: ' . $sectID . "<br />\n";
            return null;
        }

        $sectData = $arSectLink[$sectID];
        $linkPath = explode('/', $sectData['link']);
        $code = $linkPath[count($linkPath) - 2];

        print_r($sectData);
        print_r('code of linked section: ' . $code . "<br />\n");
        $log .= 'code of linked section: ' . $code . "<br />\n";

        if ($code) {
            $arFilter = ["IBLOCK_ID" => self::CATALOG_IBLOCK_ID, "ACTIVE" => "Y", "CODE" => $code];
            $arSelect = ["ID"];
            $rsSect = CIBlockSection::GetList([], $arFilter, false, $arSelect);
            if ($foundedSect = $rsSect->GetNext()) {
                print_r('linked section found: ' . $foundedSect['ID'] . "<br />\n");
                $log .= 'linked section found: ' . $foundedSect['ID'] . "<br />\n";
                $resolveID = $foundedSect['ID'];
                $createDepth = intVal($sectData['create']);
                print_r('dept: ' . $createDepth . "<br />\n");
                $log .= 'dept: ' . $createDepth . "<br />\n";
                $namePath = explode('/', $sectData['path']);
                while ($createDepth > 0) {
                    $newSectName = trim($namePath[count($namePath) - $createDepth]);
                    print_r('new section name: ' . $newSectName . "<br />\n");
                    $log .= 'new section name: ' . $newSectName . "<br />\n";

                    $bs = new CIBlockSection;

                    $arTranslit = $this->getSectTranslitOptions();
                    print_r('translit opt: ' . json_encode($arTranslit) . "<br />\n");
                    $log .= 'translit opt: ' . json_encode($arTranslit) . "<br />\n";
                    $translitOpt = [
                        "max_len" => $arTranslit["TRANS_LEN"],
                        "change_case" => $arTranslit["TRANS_CASE"],
                        "replace_space" => $arTranslit["TRANS_SPACE"],
                        "replace_other" => $arTranslit["TRANS_OTHER"],
                    ];
                    $code = Cutil::translit($newSectName, "ru", $translitOpt);
                    print_r('new section code: ' . $code . "<br />\n");
                    $log .= 'new section code: ' . $code . "<br />\n";

                    // Может такой каталог уже создан
                    $arFindFilter = [
                        "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                        "ACTIVE" => "Y",
                        "IBLOCK_SECTION_ID" => $resolveID,
                        "CODE" => $code
                    ];
                    $arFindSelect = ["ID"];
                    $rsFindSect = CIBlockSection::GetList([], $arFindFilter, false, $arFindSelect);
                    if ($findSect = $rsFindSect->GetNext()) {
                        print_r('section already created: ' . $findSect['ID'] . "<br />\n");
                        $log .= 'section already created: ' . $findSect['ID'] . "<br />\n";
                        $createDepth--;
                        $resolveID = $findSect['ID'];
                        continue;
                    }

                    // Создание каталога
                    $arFields = [
                        "ACTIVE" => "Y",
                        "IBLOCK_SECTION_ID" => $resolveID,
                        "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                        "NAME" => $newSectName,
                        "CODE" => $code,
                        "CREATED_BY" => self::MODIFY_USER_ID,
                        "MODIFIED_BY" => self::MODIFY_USER_ID,
                    ];
                    $ID = $bs->Add($arFields);
                    if ($ID > 0) {
                        print_r('new section created: ' . $ID . "<br />\n");
                        $log .= 'new section created: ' . $ID . "<br />\n";
                        $createDepth--;
                        $resolveID = $ID;
                    } else {
                        print_r('new section error: ');
                        $log .= 'new section error: ';
                        echo $bs->LAST_ERROR;
                        $log .= $bs->LAST_ERROR;
                        $log .= "\n";
                        $this->saveTmpLog($log);
                        return null;
                    }
                }
                $this->saveTmpLog($log);
                return $resolveID;
            }
        }

        return null;
    }

    public function tmpResolveRootPublic(int $sectID): void
    {
        $restore = $this->restoreState();
        $this->resolveRootSectByData($sectID);
    }

    private function resolveRootSectByData(int $sCatId): ?int
    {
        if (!isset($this->arRootSectLink)) {
            $this->arRootSectLink = $this->loadRootSectLinkData();
        }
        $arRootSectLink = $this->arRootSectLink;

        // Поиск корневого раздела в дереве самсона
        $rootSect = $this->getSamsonRootSection($sCatId);
        if (is_array($rootSect)) {
            // Сопоставление с разделом на сайте
            foreach ($arRootSectLink as $key => $sect) {
                if ($sect['name'] == $rootSect['name']) {
                    // Поиск раздела по коду
                    $linkPath = explode('/', $sect['link']);
                    $code = $linkPath[count($linkPath) - 2];
                    if ($code) {
                        $arFilter = ["IBLOCK_ID" => self::CATALOG_IBLOCK_ID, "ACTIVE" => "Y", "CODE" => $code];
                        $arSelect = ["ID"];
                        $rsSect = CIBlockSection::GetList([], $arFilter, false, $arSelect);
                        if ($foundedSect = $rsSect->GetNext()) {
                            return $foundedSect['ID'];
                        }
                    }
                }
            }
        }

        return null;
    }

    private function saveTmpLog(string $log): void
    {
        $fn = $this->tmp_dir . '/log.dat';
        $file = fopen($fn, "a");
        fwrite($file, $log);
        fclose($file);
    }

    private function loadSectLinkData(): array
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");
        $csvFile = new CCSVData('R', false);
        $csvFile->LoadFile($this->sectLinkDatafile);
        $csvFile->SetDelimiter(';');
        $arSectLink = [];
        while ($data = $csvFile->Fetch()) {
            $arSectLink[$data[0]] = [
                'id' => trim($data[0]),
                'name' => trim($data[1]),
                'path' => trim($data[2]),
                'link' => trim($data[3]),
                'create' => trim($data[4]),
            ];
        }
        return $arSectLink;
    }

    private function loadRootSectLinkData(): array
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");
        $csvFile = new CCSVData('R', false);
        $csvFile->LoadFile($this->rootSectLinkDatafile);
        $csvFile->SetDelimiter(';');
        $arSectLink = [];
        while ($data = $csvFile->Fetch()) {
            $arSectLink[] = [
                'name' => trim($data[0]),
                'link' => trim($data[1]),
            ];
        }
        return $arSectLink;
    }

    private function loadIgnoreData(): void
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");

        // Articles
        $csvFile = new CCSVData('R', false);
        $csvFile->LoadFile($this->ignoreFile);
        $csvFile->SetDelimiter(';');
        $ignoreItems = [];
        while ($data = $csvFile->Fetch()) {
            $ignoreItems[] = trim($data[0]);
        }
        $this->ignore = $ignoreItems;

        // Brands
        $csvFile = new CCSVData('R', false);
        $csvFile->LoadFile($this->ignoreBrandFile);
        $csvFile->SetDelimiter(';');
        $ignoreBrands = [];
        while ($data = $csvFile->Fetch()) {
            $ignoreBrands[] = mb_strtolower(trim($data[0]));
        }
        $this->ignoreBrand = $ignoreBrands;
    }

    private function convertSamsonProperties(array $item): array
    {
        $catalogProperties = $this->getCatalogProperties();
        $propertiesEnumValues = $this->getCatalogEnumValues();

        $properties = [];

        $addPhoto = array_slice($item['photo_list'], 1);
        $addPropPhoto = [];
        foreach ($addPhoto as $photo) {
            $addPropPhoto[] = CFile::MakeFileArray($photo);
        }
        $properties[self::MORE_PHOTO_PROPERTY_CODE] = $addPropPhoto;

        $additionalProps = [
            ["name" => "Бренд", "value" => $item['brand']],
            ["name" => "Производитель", "value" => $item['manufacturer']],
            ["name" => "Артикул", "value" => trim($item['sku']) . "С"],
        ];
        // Проверка минимальной партии
        if ($item['package_list']['type'] == "min_kor") {
            $additionalProps[] = ["name" => "Минимальная партия КОР", "value" => $item['package_list']['value']];
        }

        $propsToConvert = $this->matchFacetWithAdditional($item['facet_list'], $additionalProps);
        foreach ($propsToConvert as $prop) {
            $search = ['Количество'];
            $replace = ['Кол-во'];
            $propName = str_replace($search, $replace, $prop['name']);

            $ignore = ['Длина', 'Ширина', 'Высота', 'Объем', 'Вес'];
            if (in_array($propName, $ignore)) {
                continue;
            }

            $pattern = ["/^\"/", "/\"$/"];
            $replace = ["", ""];
            $propValue = preg_replace($pattern, $replace, $prop['value']);

            if (!$propName || !$propValue) {
                continue;
            }

            foreach ($catalogProperties as $key => $catProp) {
                if (trim(mb_strtolower($propName)) == trim(mb_strtolower($catProp['NAME']))) {
                    // Найдено свойство
                    $valueFouded = false;
                    if ($catProp['PROPERTY_TYPE'] == 'L') {
                        $arEnumValue = $propertiesEnumValues[$catProp['ID']];
                        foreach ($arEnumValue as $propID => $catPropValue) {
                            if (trim(mb_strtolower($catPropValue['VALUE'])) == trim(mb_strtolower($propValue))) {
                                if (!isset($properties[$catProp['ID']])) {
                                    $properties[$catProp['ID']] = [];
                                }
                                $properties[$catProp['ID']][] = $catPropValue['ID'];
                                $valueFouded = true;
                                break;
                            }
                        }
                        if (!$valueFouded) {
                            // Добавляем новое значение свойства

                            $sort = intVal($arEnumValue[count($arEnumValue) - 1]['SORT']) + 100;

                            $ibpenum = new CIBlockPropertyEnum;
                            $enum = [
                                "PROPERTY_ID" => $catProp['ID'],
                                "VALUE" => $propValue,
                                "SORT" => $sort,
                            ];
                            if ($PropEnumID = $ibpenum->Add($enum)) {
                                $enum['ID'] = $PropEnumID;
                                $this->addCatalogEnumValues($catProp['ID'], $enum);

                                if (!isset($properties[$catProp['ID']])) {
                                    $properties[$catProp['ID']] = [];
                                }
                                $properties[$catProp['ID']][] = $PropEnumID;
                            }
                        }
                    } else {
                        if (!isset($properties[$catProp['ID']])) {
                            $properties[$catProp['ID']] = [];
                        }
                        $properties[$catProp['ID']][] = $prop['value'];
                    }
                }
            }
        }

        return $properties;
    }

    private function matchFacetWithAdditional(array $facet, array $add): array
    {
        $matched = [];
        foreach ($facet as $key => &$facet_prop) {
            foreach ($add as $addkey => $add_prop) {
                if ($add_prop["name"] === $facet_prop["name"]) {
                    $facet_prop = $add_prop;
                    $matched[] = $addkey;
                }
            }
        }
        foreach ($matched as $index) {
            unset($add[$index]);
        }
        return array_merge($facet, $add);
    }

    private function getBasePriceId(): int
    {
        if (!$this->basePriceId) {
            $prices = CCatalogGroup::GetBaseGroup();
            if (is_array($prices)) {
                $this->basePriceId = $prices['ID'];
            }
        }

        return $this->basePriceId;
    }

    private function getCatalogProperties(): ?array
    {
        if (!CModule::IncludeModule('iblock')) {
            return null;
        }

        if (isset($this->catalogProperties) && count($this->catalogProperties)) {
            return $this->catalogProperties;
        }

        $properties = [];
        $rsProperties = CIBlockProperty::GetList(
            ["SORT" => "ASC"],
            ["ACTIVE" => "Y", "IBLOCK_ID" => self::CATALOG_IBLOCK_ID]
        );
        while ($arProp = $rsProperties->GetNext()) {
            $properties[] = [
                "ID" => $arProp["ID"],
                "CODE" => $arProp["CODE"],
                "NAME" => $arProp["NAME"],
                "PROPERTY_TYPE" => $arProp["PROPERTY_TYPE"],
                "SORT" => $arProp["SORT"],
            ];
        }
        $this->catalogProperties = $properties;
        return $properties;
    }

    private function getCatalogEnumValues(): array
    {
        if (!CModule::IncludeModule('iblock')) {
            return null;
        }

        if (isset($this->catalogEnumValues) && count($this->catalogEnumValues)) {
            return $this->catalogEnumValues;
        }

        $propertyEnums = [];
        $rsPropertyEnums = CIBlockPropertyEnum::GetList(
            ["SORT" => "ASC"],
            ["ACTIVE" => "Y", "IBLOCK_ID" => self::CATALOG_IBLOCK_ID]
        );
        while ($arEnumValue = $rsPropertyEnums->GetNext()) {
            $enum = [
                "ID" => $arEnumValue["ID"],
                "PROPERTY_ID" => $arEnumValue["PROPERTY_ID"],
                "VALUE" => $arEnumValue["VALUE"],
                "SORT" => $arEnumValue["SORT"],
            ];
            if (!isset($propertyEnums[$enum['PROPERTY_ID']])) {
                $propertyEnums[$enum['PROPERTY_ID']] = [];
            }
            $propertyEnums[$enum['PROPERTY_ID']][] = $enum;
        }

        $this->catalogEnumValues = $propertyEnums;
        return $propertyEnums;
    }

    private function addCatalogEnumValues(int $id, array $enum): void
    {
        $this->catalogEnumValues[$id][] = $enum;
    }

    private function requestTranslitOptions(): void
    {
        $arIBlock = CIBlock::GetArrayByID(self::CATALOG_IBLOCK_ID);
        if ($arIBlock['ID']) {
            $arTranslit = $arIBlock['FIELDS']['CODE']['DEFAULT_VALUE'];
            $arSectTranslit = $arIBlock['FIELDS']['SECTION_CODE']['DEFAULT_VALUE'];
            $this->arTranslit = $arTranslit;
            $this->arSectTranslit = $arSectTranslit;
        }
    }

    private function getTranslitOptions(): ?array
    {
        if (!isset($this->arTranslit) || !count($this->arTranslit)) {
            $this->requestTranslitOptions();
        }
        if (isset($this->arTranslit) && count($this->arTranslit)) {
            return $this->arTranslit;
        }
        return null;
    }

    private function getSectTranslitOptions(): ?array
    {
        if (!isset($this->arSectTranslit) || !count($this->arSectTranslit)) {
            $this->requestTranslitOptions();
        }
        if (isset($this->arSectTranslit) && count($this->arSectTranslit)) {
            return $this->arSectTranslit;
        }
        return null;
    }

    private function formatResult(array $item, array $res): array
    {
        $result = [
            'ID' => $res['ID'],
            'NAME' => $item['name'],
            'SECTION_ID' => $res['IBLOCK_SECTION_ID'],
            'URL' => $res['DETAIL_PAGE_URL'],
            'STATUS' => $res['status']
        ];
        if ($res['PRICE']) {
            $result['PRICE'] = $res['PRICE'];
        }
        if ($res['QUANTITY']) {
            $result['QUANTITY'] = $res['QUANTITY'];
        }
        if ($res['msg']) {
            $result['MSG'] = $res['msg'];
        }
        if ($res['ARTICLE']) {
            $result['ARTICLE'] = $res['ARTICLE'];
        }
        return $result;
    }

    private function loadLogFiles(): array
    {
        $arTypes = ['update', 'disable', 'add'];
        $log = [];
        foreach ($arTypes as $type) {
            $list = scandir($this->logDir);
            foreach ($list as $key => $fn) {
                if (strpos($fn, $type) !== false) {
                    $content = file_get_contents($this->logDir . '/' . $fn);
                    $obj = json_decode($content, true);

                    if (!isset($log[$type])) {
                        $log[$type] = [];
                    }
                    $log[$type] = array_merge($log[$type], $obj);
                }
            }
        }

        return $log;
    }

    private function renderReportSectionRow(int $sectID, int $colspan = 5): string
    {
        $sectionChain = $this->getSectionsNameChain($sectID);
        return "<tr class='table-info'><td colspan='{$colspan}'>{$sectionChain}</td></tr>";
    }

    private function renderReportRow(array $item, int $index, bool $moreInfo = true): string
    {
        $content = "<tr>";
        $content .= "<td>{$index}</td>";
        $content .= "<td><a target='_blank' href='https://ofisshop.ru{$item['URL']}'>{$item['NAME']}</a></td>";
        if ($item['ARTICLE']) {
            $content .= "<td>{$item['ARTICLE']}</td>";
        }
        if ($item['MSG']) {
            $content .= "<td>{$item['MSG']}</td>";
        }
        if ($moreInfo) {
            $price = number_format($item['PRICE'], 2, '.', '');
            $content .= "<td>{$price}</td>";
        }
        if ($moreInfo) {
            $content .= "<td>{$item['QUANTITY']}</td>";
        }
        $content .= "</tr>";
        return $content;
    }

    private function saveReport(): void
    {
        $report = [
            'update' => [],
            'disable' => [],
            'add' => [],
            'error' => [],
        ];
        $counts = [
            'update' => 0,
            'disable' => 0,
            'add' => 0,
            'error' => 0,
        ];

        $log = $this->loadLogFiles();

        foreach ($log as $type => $items) {
            usort($items, function ($a, $b) {
                if ($a['SECTION_ID'] == $b['SECTION_ID']) {
                    return 0;
                }
                return ($a['SECTION_ID'] < $b['SECTION_ID']) ? -1 : 1;
            });

            $log[$type] = $items;
        }

        foreach ($log as $type => $items) {
            $currentSection = 0;
            foreach ($items as $i => $item) {
                // disable or error can be in status
                $rowType = $item['STATUS'] == "success" ? $type : $item['STATUS'];

                $counts[$rowType] += 1;
                $moreInfo = $rowType == 'update' || $rowType == 'add';
                $colspan = 2;
                if ($rowType == 'update') {
                    $colspan = 5;
                } else {
                    if ($rowType == 'disable') {
                        $colspan = 4;
                    } else {
                        if ($rowType == 'add') {
                            $colspan = 4;
                        }
                    }
                }

                if ($currentSection != $item['SECTION_ID']) {
                    $currentSection = $item['SECTION_ID'];
                    $report[$rowType][] = $this->renderReportSectionRow($item['SECTION_ID'], $colspan);
                }

                $report[$rowType][] = $this->renderReportRow($item, $counts[$rowType], $moreInfo);
            }
        }

        $reportTPL = file_get_contents($this->reportTemplate);
        if ($reportTPL) {
            $search = [
                "#DATE#",
                "#UPDATE#",
                "#DISABLE#",
                "#ADD#",
                "#ERROR#",
                "#UPDATE_COUNT#",
                "#DISABLE_COUNT#",
                "#ADD_COUNT#",
                "#ERROR_COUNT#"
            ];
            $replace = [
                date("Y.m.d"),
                isset($report['update']) ? implode("\n", $report['update']) : '',
                isset($report['disable']) ? implode("\n", $report['disable']) : '',
                isset($report['add']) ? implode("\n", $report['add']) : '',
                isset($report['error']) ? implode("\n", $report['error']) : '',
                $counts['update'],
                $counts['disable'],
                $counts['add'],
                $counts['error'],
            ];
            $reportBuilded = str_replace($search, $replace, $reportTPL);

            $fn = $this->reportDir . "/report." . date("Y-m-d") . ".html";
            file_put_contents($fn, $reportBuilded);
        }
    }

    private function saveReportAbort()
    {
        $reportTPL = file_get_contents($this->reportTemplate);
        if ($reportTPL) {
            $search = [
                "#DATE#",
                "#UPDATE#",
                "#DISABLE#",
                "#ADD#",
                "#ERROR#",
                "#UPDATE_COUNT#",
                "#DISABLE_COUNT#",
                "#ADD_COUNT#",
                "#ERROR_COUNT#"
            ];
            $replace = [
                date("Y.m.d"),
                "<tr class='table-info'><td colspan='4'>Обновление было прервано: ошибка загрузки номенклатуры</td></tr>",
                "<tr class='table-info'><td colspan='2'>Обновление было прервано: ошибка загрузки номенклатуры</td></tr>",
                "<tr class='table-info'><td colspan='4'>Обновление было прервано: ошибка загрузки номенклатуры</td></tr>",
                "<tr class='table-info'><td colspan='2'>Обновление было прервано: ошибка загрузки номенклатуры</td></tr>",
                0,
                0,
                0,
                0,
            ];
            $reportBuilded = str_replace($search, $replace, $reportTPL);

            $fn = $this->reportDir . "/report." . date("Y-m-d") . ".html";
            file_put_contents($fn, $reportBuilded);
        }
    }

    private function getSectionsIdCnain(int $sectionID): array
    {
        $path = $this->getSectionsChain($sectionID);
        $ids = [];
        foreach ($path as $section) {
            $ids[] = $section['ID'];
        }
        return $ids;
    }

    private function getSectionsNameChain(int $sectionID): string
    {
        $path = $this->getSectionsChain($sectionID);
        $names = [];
        foreach ($path as $section) {
            $names[] = $section['NAME'];
        }
        return implode(' / ', $names);
    }

    private function getSectionsChain(int $sectionID): ?array
    {
        if (!CModule::IncludeModule('iblock')) {
            return null;
        }

        if (isset($this->sectionChain[$sectionID])) {
            $path = $this->sectionChain[$sectionID];
            return $path;
        }

        $nav = CIBlockSection::GetNavChain(self::CATALOG_IBLOCK_ID, $sectionID);
        $path = [];
        while ($arSectionPath = $nav->GetNext()) {
            $path[] = [
                'NAME' => $arSectionPath['NAME'],
                'ID' => $arSectionPath['ID'],
            ];
        }
        $this->sectionChain[$sectionID] = $path;
        return $path;
    }

    private function saveUnplacedSections(): void
    {
        $unSections = [];
        foreach ($this->samsonTree as $key => $cat) {
            if (!isset($cat['IBLOCK_SECTION_ID']) && !$this->isSamsonSectHasChild($cat['id'])) {
                $unSections[] = [
                    'id' => $cat['id'],
                    'name' => $cat['name'],
                    'path' => $this->getSamsonTreeNameChain($cat['id']),
                ];
            }
        }

        // write
        $fn = $this->reportDir . "/sections.csv";
        $file = fopen($fn, "w");
        fwrite($file, "id;name;path;" . "\n");
        foreach ($unSections as $key => $section) {
            fwrite($file, "{$section['id']};{$section['name']};{$section['path']};" . "\n");
        }
        fclose($file);
    }

    private function isSamsonSectHasChild(int $id): bool
    {
        foreach ($this->samsonTree as $key => $cat) {
            if ($cat["parent_id"] == $id) {
                return true;
            }
        }
        return false;
    }

    private function getSamsonTreeNameChain(int $catID, int $dept = 6): string
    {
        $id = $catID;
        $arNamePath = [];
        $i = 0;
        $maxCount = $dept * count($this->samsonTree);
        do {
            foreach ($this->samsonTree as $key => $cat) {
                if ($cat["id"] == $id) {
                    $arNamePath[] = $cat['name'];
                    $id = $cat["parent_id"];
                    break;
                }
                $i++;
            }
        } while ($id != 0 && $i <= $maxCount);

        $arNamePath = array_reverse($arNamePath);
        return implode(' / ', $arNamePath);
    }

    private function getSamsonRootSection(int $catID, int $dept = 6): ?array
    {
        $id = $catID;
        $maxCount = $dept * count($this->samsonTree);
        $found = false;
        do {
            foreach ($this->samsonTree as $key => $cat) {
                if ($cat["id"] == $id) {
                    $id = $cat["parent_id"];
                    if ($id == 0) {
                        $found = $key;
                    }
                    break;
                }
                $i++;
            }
        } while ($id != 0 && $i <= $maxCount);

        if ($found) {
            return $this->samsonTree[$key];
        } else {
            return null;
        }
    }
}

?>
