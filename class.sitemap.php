<?

use \Bitrix\Main\Data\Cache;

class Sitemap
{
    static $steps = ["sections", "items", "brands", "filter"];
    static $itemsFilter = [
        "!CATALOG_PRICE_1" => false,
        ">CATALOG_PRICE_1" => "0",
        [
            "LOGIC" => "OR",
            ["!DETAIL_PICTURE" => false],
            ["!PROPERTY_MORE_PHOTO" => false],
        ]
    ];
    const SITE_URL = "https://site.ru";
    const CATALOG_IBLOCK_ID = 1;
    const CATALOG_PAGE_INDEX = 1;
    const CATALOG_PAGE_COUNT = 12;
    const BRAND_IBLOCK_ID = 2;
    const BRAND_PROPERTY_CODE = "BRAND";
    const BRAND_PAGE_INDEX = 1;
    const BRAND_PAGE_COUNT = 60;
    const BRAND_URL_TEMPLATE = "/brand/#NAME#/";

    private $step;
    private $iteration;

    private $currentTimestamp;
    private $sectionsMap;
    private $sectionsPagenMap;

    function __construct(array $state = [])
    {
        $this->currentTimestamp = time();
        $initState = $this->getInitState();
        $this->setState(array_merge($initState, $state));

        $this->sectionsMap = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/sections.txt";
        $this->sectionsPagenMap = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/sections_pagen.txt";
        $this->itemMap = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/items.txt";
        $this->brandsMap = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/brands.txt";
        $this->brandsPagenMap = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/brands_pagen.txt";
        $this->brandsSectionsMap = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/brands_sections.txt";
        $this->smartFilterMap = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/smart_filter.txt";

        $this->smartFilterMapComplete = $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/_ready/smart_filter.txt";

        if (!isset($state["step"])) {
            $this->clearMaps();
        }
    }

    private function getInitState(): array
    {
        return [
            "step" => 0,
            "iteration" => 1,
        ];
    }

    private function setState(array $state): void
    {
        $this->step = $state["step"];
        $this->iteration = $state["iteration"];
    }

    public function getState(): array
    {
        return [
            "step" => $this->step,
            "iteration" => $this->iteration,
        ];
    }

    private function setNextStep(): void
    {
        $this->step += 1;
        $this->iteration = 1;
    }

    private function setNextIteration(): void
    {
        $this->iteration += 1;
    }

    private function clearMaps(): void
    {
        if (file_exists($this->sectionsMap)) {
            unlink($this->sectionsMap);
        }
        if (file_exists($this->sectionsPagenMap)) {
            unlink($this->sectionsPagenMap);
        }
        if (file_exists($this->itemMap)) {
            unlink($this->itemMap);
        }
        if (file_exists($this->brandsMap)) {
            unlink($this->brandsMap);
        }
        if (file_exists($this->brandsPagenMap)) {
            unlink($this->brandsPagenMap);
        }
        if (file_exists($this->brandsSectionsMap)) {
            unlink($this->brandsSectionsMap);
        }
        if (file_exists($this->smartFilterMap)) {
            unlink($this->smartFilterMap);
        }
    }

    public function process(): bool
    {
        $step = self::$steps[$this->step];
        switch ($step) {
            case "sections":
            {
                $res = $this->createSectionsMap();
                if ($res) {
                    $this->setNextStep();
                }
                return false;
            }
            case "items":
            {
                $res = $this->createItemsMap();
                if ($res) {
                    $this->setNextStep();
                }
                return false;
            }
            case "brands":
            {
                $res = $this->createBrandsMap();
                if ($res) {
                    $this->setNextStep();
                }
                return false;
            }
            case "filter":
            {
                $res = $this->createSmartFilterMap();
                if ($res) {
                    $this->setNextStep();
                } else {
                    $this->setNextIteration();
                }
                return false;
            }
        }

        print_r('exec time: ' . (time() - $this->currentTimestamp) . '<br />');

        return true;
    }


    private function createSectionsMap(): ?bool
    {
        if (!CModule::IncludeModule('iblock')) {
            return null;
        }

        $arFilter = ['IBLOCK_ID' => self::CATALOG_IBLOCK_ID, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'];
        $arOrder = ["SORT" => "ASC"];
        $arSelect = ["ID", "NAME", "SECTION_PAGE_URL"];
        $arPageNav = ["nPageSize" => 100, "iNumPage" => $this->iteration];
        $rsSect = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, $arPageNav);
        $sectCount = $rsSect->SelectedRowsCount();
        $sectPageCount = ceil($sectCount / $arPageNav["nPageSize"]);
        while ($arSect = $rsSect->GetNext()) {
            $arElementsFilter = array_merge(
                [
                    "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                    "SECTION_ID" => $arSect["ID"],
                    "ACTIVE" => "Y",
                    "INCLUDE_SUBSECTIONS" => "Y"
                ],
                self::$itemsFilter
            );
            $rsElements = CIBlockElement::GetList([], $arElementsFilter, false, false, ["ID"]);
            $count = $rsElements->SelectedRowsCount();
            if ($count == 0) {
                continue;
            }

            $pageCount = ceil($count / self::CATALOG_PAGE_COUNT);

            file_put_contents($this->sectionsMap, self::SITE_URL . $arSect["SECTION_PAGE_URL"] . "\n", FILE_APPEND);
            if ($pageCount > 1) {
                for ($i = 2; $i <= $pageCount; $i++) {
                    file_put_contents(
                        $this->sectionsPagenMap,
                        self::SITE_URL . $arSect["SECTION_PAGE_URL"] . "?PAGEN_" . self::CATALOG_PAGE_INDEX . "=${i}" . "\n",
                        FILE_APPEND
                    );
                }
            }
        }

        if ($sectPageCount == $this->iteration) {
            return true;
        }
        $this->setNextIteration();
        return false;
    }

    private function createItemsMap(): ?bool
    {
        if (!CModule::IncludeModule('iblock')) {
            return null;
        }

        $arElementsFilter = array_merge(
            [
                "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                "!SECTION_ID" => false,
                "ACTIVE" => "Y",
            ],
            self::$itemsFilter
        );
        $arOrder = ["ID" => "ASC"];
        $arSelect = ["ID", "DETAIL_PAGE_URL"];
        $arPageNav = ["nPageSize" => 10000, "iNumPage" => $this->iteration];
        $rsElements = CIBlockElement::GetList([], $arElementsFilter, false, $arPageNav, $arSelect);
        $count = $rsElements->SelectedRowsCount();
        $pageCount = ceil($count / $arPageNav["nPageSize"]);
        while ($arItem = $rsElements->GetNext()) {
            file_put_contents($this->itemMap, self::SITE_URL . $arItem["DETAIL_PAGE_URL"] . "\n", FILE_APPEND);
        }

        if ($pageCount == $this->iteration) {
            return true;
        }
        $this->setNextIteration();
        return false;
    }

    private function createBrandsMap(): ?bool
    {
        if (!CModule::IncludeModule('iblock')) {
            return null;
        }

        //SELECT
        $arItemsSelect = [];

        //WHERE
        $arItemsFilter = array_merge(
            [
                "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                "IBLOCK_LID" => SITE_ID,
                "ACTIVE" => "Y",
                "!PROPERTY_" . self::BRAND_PROPERTY_CODE . "_VALUE" => false,
            ],
            self::$itemsFilter
        );

        //ORDER BY
        $arItemsSort = [
            "CNT" => "DESC"
        ];

        $arItemsGroup = ["PROPERTY_" . self::BRAND_PROPERTY_CODE];

        $arResult["BRANDS"] = [];

        $rsElement = CIBlockElement::GetList($arItemsSort, $arItemsFilter, $arItemsGroup, false, $arItemsSelect);
        $count = $rsElement->SelectedRowsCount();
        $pageCount = ceil($count / self::BRAND_PAGE_COUNT);
        file_put_contents($this->brandsPagenMap, self::SITE_URL . "/brand/" . "\n");
        for ($i = 2; $i <= $pageCount; $i++) {
            file_put_contents(
                $this->brandsPagenMap,
                self::SITE_URL . "/brand/" . "?PAGEN_" . self::CATALOG_PAGE_INDEX . "=${i}" . "\n",
                FILE_APPEND
            );
        }

        $arBrands = [];
        while ($obElement = $rsElement->GetNextElement()) {
            $arItem = $obElement->GetFields();

            $url = str_replace("#NAME#", mb_strtolower($arItem["PROPERTY_BRAND_VALUE"]), self::BRAND_URL_TEMPLATE);
            $arBrands[] = [
                "NAME" => $arItem["PROPERTY_" . self::BRAND_PROPERTY_CODE . "_VALUE"],
                "BRAND_ID" => $arItem["PROPERTY_" . self::BRAND_PROPERTY_CODE . "_ENUM_ID"],
                "CNT" => $arItem["CNT"],
                "DETAIL_PAGE_URL" => $url,
            ];
        }

        // Brands
        foreach ($arBrands as &$arBrand) {
            $arFilter = [
                "IBLOCK_ID" => self::BRAND_IBLOCK_ID,
                "IBLOCK_LID" => SITE_ID,
                "ACTIVE" => "Y",
                "NAME" => $arBrand["NAME"],
            ];
            $arSelect = ["ID", "CODE"];
            $rs = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            if ($arItem = $rs->GetNext()) {
                $url = str_replace("#NAME#", $arItem["CODE"], self::BRAND_URL_TEMPLATE);
                $arBrand["DETAIL_PAGE_URL"] = $url;
                file_put_contents($this->brandsMap, self::SITE_URL . $url . "\n", FILE_APPEND);
            }
        }

        // Brands sections
        $cTime = time();
        $i = 0;
        foreach ($arBrands as $arBrand) {
            $arFilter = array_merge(
                $arItemsFilter,
                ["PROPERTY_" . self::BRAND_PROPERTY_CODE . "_VALUE" => $arBrand["NAME"]]
            );
            $arGroup = ["IBLOCK_SECTION_ID"];
            $rsElement = CIBlockElement::GetList($arItemsSort, $arFilter, $arGroup, false, $arItemsSelect);
            while ($obElement = $rsElement->GetNextElement()) {
                $arF = $obElement->GetFields();
                $rsSect = CIBlockSection::GetByID($arF["IBLOCK_SECTION_ID"]);
                if ($arSect = $rsSect->GetNext()) {
                    file_put_contents(
                        $this->brandsSectionsMap,
                        self::SITE_URL . $arBrand["DETAIL_PAGE_URL"] . $arSect["CODE"] . "/\n",
                        FILE_APPEND
                    );
                }
            }
        }

        return true;
    }

    private function createSmartFilterMap(): ?bool
    {
        if (!CModule::IncludeModule('iblock')) {
            return null;
        }

        $sections = $this->getLastLvlSections();
        $sectGroups = array_chunk($sections, 10, true);

        global $APPLICATION;

        $groupIndex = $this->iteration - 1;

        foreach ($sectGroups[$groupIndex] as $sectId => $sect) {
            global $catalogSmartFilterResult;
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.smart.filter",
                "raw-data",
                [
                    "IBLOCK_TYPE" => "1c_catalog",
                    "IBLOCK_ID" => self::CATALOG_IBLOCK_ID,
                    "SECTION_ID" => $sect["ID"],
                    "FILTER_NAME" => "arCatalogFilter",
                    "PRICE_CODE" => "",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => 1,
                    "CACHE_GROUPS" => "N",
                    "SAVE_IN_SESSION" => "N",
                    "XML_EXPORT" => "N",
                    "SEF_MODE" => "Y",
                    "SEF_RULE" => "/catalog/#SECTION_CODE#/f/#SMART_FILTER_PATH#/",
                    "SMART_FILTER_PATH" => "",
                    "SECTION_TITLE" => "NAME",
                    "SECTION_DESCRIPTION" => "DESCRIPTION",
                    "HIDE_NOT_AVAILABLE" => "Y",
                    "DISPLAY_ELEMENT_COUNT" => "Y",
                    "NOINDEX_PROP_FILE" => $_SERVER["DOCUMENT_ROOT"] . "/upload/sitemap/_data/noindex-filter-properties.csv",
                ]
            );

            foreach ($catalogSmartFilterResult["ITEMS"] as $prodId => $prop) {
                if (is_array($prop["VALUES"]) && count($prop["VALUES"]) > 0) {
                    foreach ($prop["VALUES"] as $value) {
                        if ($value["ELEMENT_COUNT"] >= 2) {
                            file_put_contents(
                                $this->smartFilterMap,
                                self::SITE_URL . $value["VALUE_URL"] . "\n",
                                FILE_APPEND
                            );
                        }
                    }
                }
            }
        }

        if (count($sectGroups) == $this->iteration) {
            copy($this->smartFilterMap, $this->smartFilterMapComplete);
            return true;
        }

        return false;
    }

    private function getLastLvlSections(): array
    {
        $cache = Cache::createInstance();
        $idCahe = "last-lvl-sections";
        $dirCache = '/sitemap';
        $asSections = [];

        if ($cache->initCache(60 * 60 * 23, $idCahe, $dirCache)) { // проверяем кеш и задаём настройки
            $asSections = $cache->getVars(); // достаем переменные из кеша
        } elseif ($cache->startDataCache()) {
            $arFilter = ['IBLOCK_ID' => self::CATALOG_IBLOCK_ID, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'];
            $arOrder = ["DEPTH_LEVEL" => "ASC", "SORT" => "ASC"];
            $arSelect = ["ID", "IBLOCK_SECTION_ID", "NAME", "SECTION_PAGE_URL"];
            $rsSect = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);
            while ($arSect = $rsSect->GetNext()) {
                $asSections[$arSect["ID"]] = [
                    "ID" => $arSect["ID"],
                    "IBLOCK_SECTION_ID" => $arSect["IBLOCK_SECTION_ID"],
                    "NAME" => $arSect["~NAME"],
                    "SECTION_PAGE_URL" => $arSect["~SECTION_PAGE_URL"]
                ];
                if ($arSect["IBLOCK_SECTION_ID"] && isset($asSections[$arSect["IBLOCK_SECTION_ID"]])) {
                    $asSections[$arSect["IBLOCK_SECTION_ID"]]["IS_PARENT"] = "Y";
                }
            }

            $asSections = array_filter($asSections, function ($sect) {
                return !isset($sect["IS_PARENT"]);
            });

            $cache->endDataCache($asSections); // записываем в кеш
        }

        return $asSections;
    }
}

?>
